import { 
    ANIMATION_CHANGED,
    ITERATIONCOUNT_CHANGED,
    IMAGE1LOADED_CHANGED,
    IMAGE1FULLYLOADED_CHANGED,
    IMAGE_CHANGED
} from '../../actions/SplashActions/ActionType.js'


const INITIAL_STATE = { 
    animation : "fadeIn",
    image : require('../../assets/logo2.png'),
    iterationCount : 1,
    image1Loaded : 0,
    image2Loaded : 0,
    image1FullyLoaded : 0 
}

export default  (state = INITIAL_STATE, action) => {
    console.log(action)
    switch(action.type){
        case ANIMATION_CHANGED:
            return {...state , animation : action.payload}
        case ITERATIONCOUNT_CHANGED:
            return {...state , iterationCount : action.payload}
        case IMAGE1LOADED_CHANGED:
            return {...state , image1Loaded : action.payload}
        case IMAGE1FULLYLOADED_CHANGED:
            return {...state , image1FullyLoaded : action.payload}
        case IMAGE_CHANGED:
            return {...state , image : action.payload}  
            
        default:
            return state
    }
    
}