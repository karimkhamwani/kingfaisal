import {
    CONNECTION_CHANGED,
    USERNAME_CHANGED,
    SHOW_LOADER,
    VIEWACTIVE_CHANGED
}
from '../../actions/HomeActions/ActionType.js'

const INITIAL_STATE = {
    isConnected : false,
    name : 'karim',
    loading : false,
    viewactive : 'auto'
}
export default  (state = INITIAL_STATE, action) => {
    console.log(action)
    switch(action.type){
        case CONNECTION_CHANGED:
            return {...state , isConnected : action.payload}
        case USERNAME_CHANGED :
            return {...state , name : action.payload}
        case SHOW_LOADER : 
            return { ...state , loading : action.payload}
        case VIEWACTIVE_CHANGED : 
            return { ...state , viewactive : action.payload}
        default:
            return state
    }
    
}