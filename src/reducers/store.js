import { createStore ,compose ,applyMiddleware} from 'redux'
import { autoRehydrate} from 'redux-persist'
// import { AsyncStorage } from 'react-native';
import reducers from './index'
import { createLogger } from 'redux-logger'
import ReduxThunk from 'redux-thunk'
export default createStore(
  reducers,
  undefined,
  compose(applyMiddleware(ReduxThunk) ,autoRehydrate())
);


// compose(applyMiddleware(createLogger()) ,autoRehydrate())