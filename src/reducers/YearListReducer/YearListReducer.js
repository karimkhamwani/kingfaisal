import {
    YEARLISTDATA_CHANGED,
    YEARLISTDATA_CHANGEDAR
}
from '../../actions/HomeActions/ActionType.js'
import {
    YEARDETAILDATA_CHANGED,
    YEARDETAILVALUE_CHANGED,
    TEMP_CHANGED
}
from '../../actions/YearActions/ActionType.js'
const INITIAL_STATE = { 
    data : [],
    yeardetaildata : null,
    yearname : null,
    dataar :[]
}

export default  (state = INITIAL_STATE, action) => {
    console.log(action)
    switch(action.type){
        case YEARLISTDATA_CHANGED :
            return { ...state , data : action.payload}
        case YEARLISTDATA_CHANGEDAR:
            console.log('event fired')
            return { ...state , dataar : action.payload}
        case YEARDETAILDATA_CHANGED :
            let a = state.yearname
            return { ...state , [a]: action.payload}
        case YEARDETAILVALUE_CHANGED :
            return { ... state , yearname : action.payload}
        case TEMP_CHANGED : 
            return { ...state , temp : action.payload}
        default:
            return state
    }
    
}