import { combineReducers } from 'redux'
import navReducer from '../navigation/reducer'
import SplashReducer from './SplashReducer/SplashReducer'
import HomeReducer from './HomeReducer/HomeReducer'
import YearListReducer from './YearListReducer/YearListReducer'

export default combineReducers({
    nav : navReducer,
    splash : SplashReducer,
    home : HomeReducer,
    yearlist: YearListReducer
});