import { StackNavigator } from 'react-navigation';
import Splash from '../../src/components/splash/splash'
import SelectLanguage from '../../src/components/selectlanguage/selectlanguage'
import Home from '../../src/components/home/home'
import YearDetail from '../../src/components/yeardetail/yeardetail'



export default Navigator = StackNavigator({
  SplashScreen: {
    screen: Splash,
    navigationOptions : {
      header : null
    }
  },
  SelectLanguageScreen: {
    screen: SelectLanguage,
    navigationOptions : {
      header : null
    }
  },
  HomeScreen: {
    screen: Home,
    navigationOptions : {
      header : null
    }
  },
  YearDetailScreen: {
    screen: YearDetail,
    navigationOptions : {
      header : null
    }
  }
});


