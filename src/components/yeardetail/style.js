import styled from 'styled-components/native';

export const Container = styled.View`
  flex:1
`;

export const HeaderCircle = styled.View`
  flex:1 
  justifyContent:flex-end
  alignItems:center
`;


export const InnerCircle = styled.View`
  width:65 
  height:65
  borderWidth:1.2
  borderRadius:40
  borderColor:gray
  backgroundColor:white
`;

export const Box = styled.View`
  flex:1 
  justifyContent:center
  alignItems:center
  backgroundColor:transparent
`;

export const LineWrapper = styled.View`
  flex:0.175
`;

export const LineArea = styled.View`
  flex:1
  alignItems:center
`;

export const Line = styled.View`
  flex:1 
  width:1 
  height:10 
  backgroundColor:gray
`;


