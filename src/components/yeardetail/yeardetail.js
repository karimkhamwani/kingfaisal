import React, { Component } from 'react';
import { Text, View, ImageBackground, Dimensions, SectionList, Animated, Easing } from 'react-native';
import Header from '../../common/header/header'
import ContentHeader from '../../common/content/content'
import Description from '../../common/description/description'
import {
    Container,
    HeaderCircle,
    InnerCircle,
    Box,
    LineWrapper,
    LineArea,
    Line
} from './style.js'
import {
    onGetYearDetail
} from '../../actions/YearActions/YearActions.js'
var window = Dimensions.get('window');
import { connect } from 'react-redux'
import ConnectionNotice from '../../common/connectiontoast/connectiontoast'
import Spinner from '../../common/Spinner/Spinner'
import AnimatableView from '../../common/animation/AnimatableView'

class YearDetail extends Component {

    constructor({ navigation }) {
        super();


    }

    componentDidMount() {
        Animated.timing(this.circleAnimatedValue, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true
        }).start()
    }


    renderHeader = (section) => {
        // console.log(section.data[0].one[0].text_one)
        // const title = section.data[0].one[0].text_one
        const title = section.data[0].text_one
        console.log(section)
        return <ContentHeader title={title} />
    }

    renderItem = (section, index) => {
        console.log(section)
        // <Description />
        const texttwo = section.data[0].text_two
        const textthree = section.data[0].text_three
        return <Description texttwo={texttwo} textthree={textthree} />
    }
    showLoader() {
        const { loading } = this.props.home
        if (loading) {
            return (
                <Spinner />
            )
        }
    }

    componentWillReceiveProps(newprops) {
        const selectedYear = this.props.navigation.state.params.selectedYear
        if (this.props.home.isConnected == false && newprops.home.isConnected == true &&!this.props.yearlist[selectedYear]) {
            // this.props.onGetYearList()
            const lang = this.props.navigation.state.params.lang
            this.props.onGetYearDetail(selectedYear , lang)
        }
    }

    componentWillMount() {
        const selectedYear = this.props.navigation.state.params.selectedYear
        if (this.props.home.isConnected && !this.props.yearlist[selectedYear]) {
            const lang = this.props.navigation.state.params.lang
            this.props.onGetYearDetail(selectedYear , lang)
        }
        this.circleAnimatedValue = new Animated.Value(0)
    }

    render() {
        const selectedYear = this.props.navigation.state.params.selectedYear
        const { viewactive } = this.props.home
        console.log("year detaul data", this.props.yearlist[selectedYear])
        return (
            <View style={{ flex: 1 }} pointerEvents={viewactive}>
                <Header headerText={selectedYear} showBackButton={1} onPress={() => this.props.navigation.goBack()} />
                <ImageBackground
                    source={require('../../assets/background_home.png')}
                    style={{ flex: 1 }}>
                    <Container>
                        <HeaderCircle>
                            <Animated.View style={{ transform: [{ scaleX: this.circleAnimatedValue }] }}>

                                <InnerCircle>
                                    <Box>
                                        <AnimatableView
                                            animation="pulse"
                                            easing="linear"
                                            iterationCount="infinite">
                                            <Text style={{ fontWeight: 'bold', fontFamily: 'Merriweather-Bold' }}>{selectedYear}</Text>
                                        </AnimatableView>
                                    </Box>
                                </InnerCircle>
                            </Animated.View>
                        </HeaderCircle>

                        <LineWrapper>
                            <LineArea>
                                <Line />
                            </LineArea>
                        </LineWrapper>

                        <View style={{ flex: 6, backgroundColor: 'transparent' }}>

                            <View style={{ flex: 1 }}>

                                <View style={{ width: window.width - 20, height: window.height - 175, borderWidth: 1.2, borderRadius: 2, borderColor: 'gray', backgroundColor: 'white', left: 10 }}>
                                    {(this.props.home.isConnected || this.props.yearlist[selectedYear] != undefined) && this.props.home.loading == false ?
                                        (
                                            <SectionList
                                                renderSectionHeader={({ section }) => this.renderHeader(section)}
                                                renderItem={({ section, index }) => this.renderItem(section, index)}
                                                stickySectionHeadersEnabled={false}
                                                keyExtractor={(item, index) => index}
                                                sections={this.props.yearlist[selectedYear] != undefined ? this.props.yearlist[selectedYear] : this.props.yearlist.temp}
                                            />
                                        )
                                        : this.props.home.isConnected == false ?
                                            (
                                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text>No Internet Connection</Text>
                                                </View>
                                            ) : null
                                    }
                                </View>
                            </View>
                        </View>
                        {this.showLoader()}
                    </Container>
                </ImageBackground>
            </View>
        );
    }
}

const mapStateToProps = ({ home, yearlist }) => {
    return { home, yearlist }
}
// export default YearDetail;
export default connect(mapStateToProps, {
    onGetYearDetail
})(YearDetail);
