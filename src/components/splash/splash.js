import React, { Component } from 'react';
import { StyleSheet, Text, View , Image} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { 
  animationChanged,
  iterationCountChanged,
  image1LoadedChanged,
  image1LoadedFullyChanged,
  imageChanged
  } from '../../actions/SplashActions/SplashActions'

import AnimatableImageView from '../../common/animation/AnimatableImageView'


class Splash extends Component {
  
  constructor({ navigation }){
    super()
  }

  onImageAnimatedCompleted = () => {
    const { animation ,  iterationCount , image , image1Loaded , image2Loaded ,image1FullyLoaded} = this.props.splash
    console.log('called?')
   if(image1Loaded == 0){      
      this.props.iterationCountChanged(1)
      this.props.animationChanged("pulse")
      this.props.image1LoadedChanged(1)
    }
    else if(image1Loaded == 1){
      this.props.iterationCountChanged(1)
      this.props.animationChanged("bounceOut")
      this.props.image1LoadedChanged(2)
    }
    else if (image1Loaded == 2){
      this.props.image1LoadedFullyChanged(1)
      this.props.imageChanged(require('../../assets/logo.png'))
      this.props.iterationCountChanged(1)
      this.props.animationChanged("zoomIn")
    }
  }

  onSecondImageAnimatedCompleted = () => {
    console.log('second completed two times?')
    this.props.iterationCountChanged("infinite")
    this.props.animationChanged("pulse")
  }
  componentDidMount() {
    setTimeout(() => {
      const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'SelectLanguageScreen' })
            ]
        })
        this.props.navigation.dispatch(resetAction)
    },6000)
  }
  renderAnimatedView = () => {
    const { animation ,  iterationCount , image , image1Loaded , image2Loaded ,image1FullyLoaded} = this.props.splash
    if(image1FullyLoaded == 0){
     return (
        <AnimatableImageView 
          animation={animation} 
          iterationCount={iterationCount}
          style={{width:250,height:250}}
          easing="ease-in" 
          onAnimationEnd={this.onImageAnimatedCompleted.bind(this)} 
          image={image}
        />
     )
    }
    else{
      return (
        <AnimatableImageView 
          animation={animation}
          easing="ease-in" 
          iterationCount={iterationCount}
          style={{width:250,height:250}} 
          onAnimationEnd={this.onSecondImageAnimatedCompleted.bind(this)} 
          image={image}
        />
      )
    }
  }
 
  render() {
    const { animation ,  iterationCount , image , image1Loaded , image2Loaded ,image1FullyLoaded} = this.props.splash
    return (
        <View style={styles.container}>
          {this.renderAnimatedView()}
        </View>
    );
  }
}


const mapStateToProps = ({splash}) => {
   return {splash}
}

export default connect(mapStateToProps,{
    animationChanged,
    iterationCountChanged,
    image1LoadedChanged,
    image1LoadedFullyChanged,
    imageChanged
})(Splash);
// export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'white'
  }
})
