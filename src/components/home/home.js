import React, { Component } from 'react';
import { Animated, Easing, NetInfo, View, ImageBackground, Image, SectionList, AsyncStorage } from 'react-native';
import { connect } from 'react-redux'
import {
    onConnectionChanged,
    onUsernameChanged,
    onViewIsActiveChanged,
    onGetYearList,
    getAllYears
}
    from '../../actions/HomeActions/HomeActions.js'
import Header from '../../common/header/header'
import ConnectionNotice from '../../common/connectiontoast/connectiontoast'
import Year from '../../common/year/year'
import Circle from '../../common/circle/circle'
import AnimatableView from '../../common/animation/AnimatableView'
import Spinner from '../../common/Spinner/Spinner'
import {
    onGetYearDetail
} from '../../actions/YearActions/YearActions.js'




class Home extends Component {

    constructor({ navigation }) {
        super()
        this.yearAnimatedValue = new Animated.Value(500)
        this.state = {
            call: true,
            lang: null
        }
    }
    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
        NetInfo.isConnected.fetch().done((isConnected) => { this.props.onConnectionChanged(isConnected) });
        Animated.timing(this.yearAnimatedValue, {
            toValue: 0,
            duration: 2500,
            easing: Easing.ease
        }).start()
        AsyncStorage.getItem('lang').then((value) => {
            console.log('lang is ', value)
        })
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    }
    componentWillMount() {
        this.animatedValue = new Animated.Value(0)
        AsyncStorage.getItem('lang').then((value) => {
            this.setState({
                lang: value
            })
        })
    }

    handleConnectionChange = (isConnected) => {
        this.props.onConnectionChanged(isConnected)
        Animated.timing(this.animatedValue, {
            toValue: 25,
            duration: 1000,
            easing: Easing.ease
        }).start()
        if (this.props.home.isConnected == true) {
            setTimeout(() => {
                Animated.timing(this.animatedValue, {
                    toValue: 0,
                    duration: 1000,
                    easing: Easing.ease
                }).start()
            }, 2000)
        }
        // this.setState({show : isConnected}) 
    }

    onYearClicked = (a, selectedYear) => this.props.navigation.navigate('YearDetailScreen', { selectedYear: selectedYear, lang: this.state.lang })
    // onYearClicked = (a,b) => console.log(b)

    showLoader() {
        const { loading } = this.props.home
        if (loading) {
            return (
                <Spinner />
            )
        }
    }


    renderHeader = (headerItem) => {
        return <Year year={headerItem.key} />
    }
    renderItem = (item, index) => {
        const length = item.data.length
        const year = item.data[index]
        // this.props.onGetYearDetail(year)
        // if (length - index == 1) {
        //     return (
        //         <Circle isLastItem={true} year={year} onPress={() => this.onYearClicked(this, year)} />
        //     )
        // }
        if (year == 1979 || year == "١٩٧٩") {
            return (
                <Circle isLastItem={true} year={year} onPress={() => this.onYearClicked(this, year)} />
            )
        }
        return <Circle year={year} onPress={() => this.onYearClicked(this, year)} />
    }


    componentWillReceiveProps(newprops) {
        console.log('len of arabic', this.props.yearlist.data.length == 0)
        if (this.state.lang == 'en') {
            if (this.props.home.isConnected == false && newprops.home.isConnected == true && this.props.yearlist.data.length == 0) {
                this.props.onGetYearList(this.state.lang)
                this.props.getAllYears(this.state.lang)
            }
        }
        else {
            if (this.props.home.isConnected == false && newprops.home.isConnected == true && this.props.yearlist.dataar.length == 0) {
                this.props.onGetYearList(this.state.lang)
                this.props.getAllYears(this.state.lang)
            }
        }
    }

    render() {
        const { viewactive, isConnected } = this.props.home
        const animatedStyle = { height: this.animatedValue }
        return (
            <View style={{ flex: 1 }} pointerEvents={viewactive}>
                <Header headerText="Home" />
                <Animated.View style={animatedStyle}>
                    <ConnectionNotice connected={this.props.home.isConnected} />
                </Animated.View >
                <View style={{ flex: 1 }}>
                    <ImageBackground
                        style={{ flex: 1 }}
                        source={require('../../assets/background_home.png')}>
                        {this.showLoader()}
                        <ImageBackground
                            style={{ flex: 1, height: 300 }}
                            source={require('../../assets/arc.png')}>

                            <View style={{ flex: 0.280, backgroundColor: 'transparent' }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>

                                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                        <Image
                                            style={{ width: 80, height: 80, marginLeft: 15 }}
                                            source={require('../../assets/forty_icon.png')} />
                                    </View>



                                    <View style={{ flex: 1 }}>

                                        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>

                                            <Image
                                                style={{ width: 80, height: 80 }}
                                                source={require('../../assets/two_fifty.png')} />

                                        </View>

                                    </View>

                                    <View style={{ flex: 1 }}>

                                        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-end' }}>

                                            <Image
                                                style={{ width: 80, height: 80 }}
                                                source={require('../../assets/forty_three_icon.png')} />

                                        </View>

                                    </View>
                                </View>
                            </View>

                            <View style={{ flex: 1 }}>
                                <AnimatableView
                                    animation="fadeInLeftBig"
                                    easing="linear"
                                    duration={1500}
                                    iterationCount={1}>
                                    <SectionList
                                        renderItem={({ section, index }) => this.renderItem(section, index)}
                                        stickySectionHeadersEnabled={false}
                                        keyExtractor={(item, index) => index}
                                        sections={this.state.lang == 'ar' ? this.props.yearlist.dataar : this.props.yearlist.data}
                                    />
                                </AnimatableView>
                            </View>

                        </ImageBackground>
                    </ImageBackground>
                </View>



            </View>
        );
    }
}

const mapStateToProps = ({ home, yearlist }) => {
    return { home, yearlist }
}

export default connect(mapStateToProps, {
    onConnectionChanged,
    onUsernameChanged,
    onGetYearList,
    onViewIsActiveChanged,
    onGetYearDetail,
    getAllYears
})(Home);
