import React, { Component } from 'react';
import { Image, View, ImageBackground, Platform, Text , AsyncStorage } from 'react-native';
import { Container } from './style'
import AnimatableImageView from '../../common/animation/AnimatableImageView'
import Button from '../../common/Button/Button'
import AnimatableView from '../../common/animation/AnimatableView'
import { I18nManager } from 'react-native';
import {
  BackgroundImageContainer,
  Logo,
  InfoBox,
  ContributionDetailBox,
  ContributionDetailText,
  ContributionBox,
  ButtonBox,
  ButtonArea,
  BottomContainer,
  ContributionText
}
  from './style'
// import RNRestart from 'react-native-restart';

class SelectLanguage extends Component {

  constructor({ navigation }) {
    super()
  }

  onEnglishLangSelect = () => {
    // I18nManager.forceRTL(false);
    // I18nManager.allowRTL(false);
    // RNRestart.Restart();
    AsyncStorage.setItem('lang' ,'en')
    this.props.navigation.navigate('HomeScreen')
  }

  onArabicLangSelect = () => {
    // I18nManager.forceRTL(true);
    // I18nManager.allowRTL(true);
    // RNRestart.Restart();
    AsyncStorage.setItem('lang' ,'ar')
    this.props.navigation.navigate('HomeScreen')
  }

  render() {
    return (
      <BackgroundImageContainer
        source={require('../../assets/background_img.png')}
      >
        <Logo>
          <View style={{ flex: 1, backgroundColor: 'transparent', justifyContent: 'flex-end' }}>
            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
              <Text style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: '#B5985B',
                fontFamily: 'Merriweather-Bold'
              }}>٤٠ عامًا من العطاء</Text>
            </View>
            <Text style={{
              paddingHorizontal: 12,
              color: 'white',
              backgroundColor: 'transparent',
              fontFamily: 'Merriweather-Regular',
              fontSize: 16,
              textAlign: 'center'
            }}>إن عظمة الأمم لا تقاس بما تملكه من وسائل الحضارة المادية، وإنما تقاس بمواقفها الإنسانية من أعمال الخير والبر</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'flex-end' }}>
            <AnimatableImageView
              animation="pulse"
              easing="ease-in"
              iterationCount="infinite"
              style={{ width: 140, height: 140 }}
              image={require('../../assets/logo3.png')}
            />
          </View>
        </Logo>
        <View style={{ flex: Platform.OS == 'ios' ? 0.65 : 0.6 }}>

          <InfoBox>
            <ContributionBox>
              <ContributionText>40 Years of Contribution</ContributionText>
            </ContributionBox>
            <ContributionDetailBox>
              <ContributionDetailText>The greatness of a nation is not measured by the size of its material possessions, but by its role in the progress of mankind</ContributionDetailText>
            </ContributionDetailBox>
          </InfoBox>

          <ButtonBox>
            <ButtonArea>
              <AnimatableView
                animation="fadeInLeft"
                easing="linear"
                iterationCount={1}>
                <Button buttonText="ENGLISH" onPress={this.onEnglishLangSelect.bind(this)} />
              </AnimatableView>
              <View style={{ marginLeft: 10 }} />
              <AnimatableView
                animation="fadeInRight"
                easing="linear"
                iterationCount={1}>
                <Button buttonText="العربية" onPress={this.onArabicLangSelect.bind(this)} />
              </AnimatableView>
            </ButtonArea>
          </ButtonBox>
        </View>
      </BackgroundImageContainer>
    )
  }


}

export default SelectLanguage