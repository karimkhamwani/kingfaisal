import styled from 'styled-components/native';
import { Platform } from 'react-native';

export const BackgroundImageContainer = styled.ImageBackground`
  flex:1
  alignSelf:stretch
  width: null
`;


export const Logo = styled.View`
  flex:1
  justifyContent:flex-end
  alignItems:center
`;

export const InfoBox = styled.View`
  flex:1
  backgroundColor:transparent
`;

export const ContributionBox = styled.View`
  flex:1 
  justifyContent:flex-end
  alignItems:center
`;

export const ContributionText = styled.Text`
  fontSize:25
  fontWeight:bold
  color:#B5985B
  fontFamily:Merriweather-Bold
`;

export const ContributionDetailBox = styled.View`
  flex:1 
  alignItems:center
`;

export const ContributionDetailText = styled.Text`
  alignSelf:center
  textAlign:center
  paddingHorizontal:12 
  color:white
  fontFamily:Merriweather-Regular
  fontSize:16
`;

export const ButtonBox = styled.View`
  flex:0.5
`;

export const ButtonArea = styled.View`
  flex:1
  justifyContent:center
  flexDirection:row
  alignItems:center
`;

export const BottomContainer = styled.View`
  flex:${Platform.select({ iOS: 0.65, android: 0.6 })};
`;