import React from 'react';
import { StyleSheet, Text, View , I18nManager ,AsyncStorage} from 'react-native';
// import i18n from './src/i18n/i18n'
import { Provider } from 'react-redux'
import { createStore , applyMiddleware} from 'redux'
import reducer from './reducers'
import RootNavigator from './navigation'
import store from './reducers/store'
import { persistStore } from 'redux-persist';
// import i18n from '../src/i18n/i18n'
import Spinner from './common/Spinner/Spinner'

import SplashScreen from 'react-native-splash-screen'


export  class App extends React.Component {

  constructor(props){
    super(props) 
    console.log("store is ",store.getState())
    this.state = {
      isReady : false
    }
  }
  componentDidMount() {
      SplashScreen.hide();
      persistStore(store,
      {
        storage : AsyncStorage,
        blacklist : ['splash' , 'nav','home']
      },
      () => this.setState({isReady : true})
      )
    }
  render() {
    // const store = createStore(reducer , {}, applyMiddleware(ReduxThunk))
    // if(!this.state.isReady){
    //   return <Spinner />
    // }
    return (
      <Provider store={store}>
        <View style={styles.container}>
            <RootNavigator/>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius:4,
    borderStyle :'solid',
  }
});



export default App;

// export {MainScreenNavigator}