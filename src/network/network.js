import { doGet } from './axiosConfig'



export const onGetYearListData = (lang) => {
    return doGet('/getYearList?lang='+lang)
    
}



export const onGetYearDetailData = (selectedYear , lang) => {    
    return doGet('/getYearDetails?year='+selectedYear+'&lang='+lang)
}


export const onGetAllYears = (lang) => {
    return doGet('/getAllYearList?lang='+lang)
}