import axios from 'axios';
import { 
    API_BASE_URL , 

  } from '../constants/routes';
import { Platform } from 'react-native';


const getHeaders = () =>{
    return{
        'Content-Type':'application/json',
        'Access_token': '8698565868'
    }
}
var authOptions = {
    method: null,
    data: null,
    headers: getHeaders() ,
    // timeout: 500
  };

export const doPost = (url,data) => {
    authOptions.method = 'POST'
    authOptions.data = data
    console.log('headers are ' , authOptions)
    return axios(API_BASE_URL+url , authOptions) 
}

export const doGet = (url) => {
    authOptions.method = 'GET'
    console.log('headers are ' , authOptions)
    return axios(API_BASE_URL+url , authOptions) 
}
