import React from 'react';
import { Text , View } from 'react-native';

const ContentHeader = ({title}) => {
    return (

        <View style={{flex:1,paddingTop:20}}>
            <Text style={{textAlign:'center' ,color:'#B5985B' , fontFamily:'Merriweather-Bold'}}>{title}</Text>
        </View>

    );
}

export default ContentHeader;