import { TouchableOpacity } from 'react-native';
import React from 'react';
const Touchable = ({onPress,children}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            {children}
        </TouchableOpacity>
    );
}

export default Touchable;