import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import {
    CircleBox,
    CircleWrapper,
    InnerBox,
    ImageBackground,
    LineWrapper,
    Line
} from './style.js'
import Touchable from '../../common/touchable/touchable'
import AnimatableView from '../../common/animation/AnimatableView'


const Circle = ({ isLastItem, onPress , year }) => {
    if (isLastItem == true) {
        return (

            <View style={{ flex: 0.2, flexDirection: 'row'}}>
                <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={onPress}>
                            <View style={{ flex: 0.5 }}>
                                    <Image
                                        source={require('../../assets/circle1.png')}
                                        style={{ width: 31, height: 31 }}
                                    />
                                <View style={{ flex: 0.5, alignItems: 'center' }}>
                                    <View style={{ height: 10, width: 2, backgroundColor: 'transparent' }} />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex:0.8}}>
                <View style={{flex:1 , backgroundColor:'transparent'}}>
                    <Text style={{fontSize:14 ,top:6 , left:5 , fontFamily:'Merriweather-Regular',color:'gray'}}>{year}</Text>
                </View>
            </View>
            </View>

        )
    }
    return (

        <View style={{ flex: 0.2, flexDirection: 'row' }}>
            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                <View style={{ flex: 1, flexDirection: 'column'  }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={onPress}>
                        <View style={{ flex: 0.5 }}>
                            
                                <Image
                                    source={require('../../assets/circle1.png')}
                                    style={{ width: 31, height: 31 }}
                                />
                        </View>
                    </TouchableOpacity>
                    <View style={{ flex: 0.5, alignItems: 'center' }}>
                        <View style={{ height: 10, width: 2, backgroundColor: 'gray' }} />
                    </View>
                </View>
            </View>
            <View style={{flex:0.8}}>
                <View style={{flex:1 , backgroundColor:'transparent'}}>
                    <Text style={{fontSize:14 ,top:6 , left:5 , fontFamily:'Merriweather-Regular',color:'gray'}}>{year}</Text>
                </View>
            </View>
        </View>

    );
}

export default Circle;