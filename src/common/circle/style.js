import styled from 'styled-components/native';


export const CircleBox = styled.View`
    flex:0.1
`;

export const CircleWrapper = styled.View`
    flex:1
    flexDirection:row
`;

export const InnerBox = styled.View`
    flex:0.240
    alignItems:flex-end
    justifyContent:flex-start
`;

export const ImageBackground = styled.View`
    flex:1
`;


export const LineWrapper = styled.View`
    flex:1
    right:13
    top:0
    backgroundColor:gray
`;

export const Line = styled.View`
    height:10
    width:2
    backgroundColor:gray
`;

