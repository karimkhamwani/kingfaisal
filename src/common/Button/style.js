import styled from 'styled-components/native';


export const Touchable = styled.TouchableOpacity`
    backgroundColor :transparent
    height:35
    borderWidth: 1
    borderColor: white
    width :150
    borderRadius : 20
    justifyContent:center
    alignItems:center
`;

export const ButtonArea = styled.View`
    alignItems :center 
    justifyContent:center
`;


export const ButtonText = styled.Text`
    fontSize:15
    color :white
    fontWeight :400
    fontFamily:Merriweather-Regular
`;