import React, { Component } from 'react';
import { Text, View } from 'react-native';
import {
    Toast
} from './style.js'

class ConnectionNotice extends Component {
    constructor(props) {
        super()
    }
    render() {
        const { connected } = this.props
        return (
            <View>
                {connected == false ?
                        <View style={{ height: 25, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white' }}>No Internet Connection </Text>
                        </View>
                    :
                        <View style={{ height: 25, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white' }}>Connected </Text>
                        </View>
                }
            </View>
        );
    }
}

export default ConnectionNotice;