import React from 'react';
import { Text, View, Image, ImageBackground , Platform} from 'react-native';
import Circle from '../../common/circle/circle'
const Year = ({year}) => {
    return (
    <View style={{flex:1 , height:80  , marginBottom:-38 }}>
        <ImageBackground
            source={require('../../assets/timeline.png')}
            resizeMode="contain"
            style={{ width: 250, left: Platform.OS == 'ios' ? 57 : 55, top: -19,flex:1 }}>
            </ImageBackground>
            <View style={{flex:1 , backgroundColor:'transparent' , position : 'absolute' , left:250,top:5}}>
                    <Text style={{fontSize:18 , color:'gray',fontFamily:'Merriweather-Regular'}}>{year}</Text>
            </View>
    </View>

    );
}

export default Year;

{/*<View style={{flex:0.1}}>
            <View style={{flex:1 , flexDirection:'row'}}>
                <View style={{flex:1 ,alignItems:'flex-start'}}>
                    <Image
                    source={require('../../assets/timeline.png')}
                    resizeMode="contain"
                    style={{width:250, left:57 , top:-20}}
                    />
                </View>
                
                <View style={{flex:1 , backgroundColor :'transparent' , alignItems:'center',right:10}}>
                    <Text style={{fontSize:18 , color:'gray' , top:5}}>1980</Text>
                </View>               
            </View>
        </View>*/}