import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';
import { Text , Image} from 'react-native';
import PropTypes from 'prop-types';


// import 
const AnimatableImageView = ({animation , iterationCount ,onAnimationEnd ,image , style,easing}) => {
    return (
        <Animatable.View 
          animation={animation} 
          easing={easing}
          iterationCount={iterationCount}
          onAnimationEnd={onAnimationEnd}
          >
          <Image
            source={image}
            resizeMode="contain"
            style={style}
          />
        </Animatable.View>
    );
}
AnimatableImageView.propTypes = {
  animation: PropTypes.string.isRequired,
  style : PropTypes.object.isRequired,
  easing: PropTypes.string.isRequired,
  image : PropTypes.number.isRequired,
  iterationCount : PropTypes.oneOfType([PropTypes.string , PropTypes.number])
};
export default AnimatableImageView;