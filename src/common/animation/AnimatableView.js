import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';
import { Text , Image} from 'react-native';
import PropTypes from 'prop-types';


// import 
const AnimatableView = ({children , animation , iterationCount , easing , duration ,onAnimationEnd}) => {
    return (
        <Animatable.View 
          animation={animation} 
          easing={easing}
          onAnimationEnd={onAnimationEnd}
          iterationCount={iterationCount}
          duration={duration}
          >
          {children}
        </Animatable.View>
    );
}
AnimatableView.propTypes = {
  animation: PropTypes.string.isRequired,
//   easing : PropTypes.string.isRequired,
  iterationCount : PropTypes.oneOfType([PropTypes.string , PropTypes.number])
};
export default AnimatableView;