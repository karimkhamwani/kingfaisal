import { SkypeIndicator } from 'react-native-indicators';
import { Loader } from './style'
import React from 'react';
import { ActivityIndicator } from 'react-native';

const Spinner = () => {
    return (
        <Loader>
            <ActivityIndicator size="large" color="#B5985B" />
        </Loader>
    )
}
export default Spinner;