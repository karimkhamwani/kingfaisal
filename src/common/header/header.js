import React from 'react';
import { Text, View, Platform, Image, TouchableOpacity } from 'react-native';
import {
    HeaderView,
    HeaderLeft,
    HeaderTitle,
    HeaderText,
    HeaderRight
} from './style.js'
import ConnectionNotice from '../../common/connectiontoast/connectiontoast'


const Header = ({ headerText, showBackButton, onPress }) => {
    state = {
        showBackButton: showBackButton || 0
    }
    console.log('header renders?')
    return (
        <HeaderView>
            <HeaderLeft theme={{ opacity: this.state.showBackButton }}>
                <TouchableOpacity onPress={onPress}>
                    <Image
                        resizeMode="contain"
                        source={require('../../assets/arrow_back_black.png')}
                        style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
            </HeaderLeft>
            <HeaderTitle>
                <HeaderText>
                    {headerText}
                </HeaderText>
            </HeaderTitle>
            <HeaderRight/>
        </HeaderView>
        
    );
};

export default Header;