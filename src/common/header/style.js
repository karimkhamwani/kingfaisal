import styled from 'styled-components/native';
import { Platform } from 'react-native';

export const HeaderView = styled.View`
    backgroundColor:#B5985B
    alignItems:center
    shadowColor: #000
    shadowOpacity: 0.2
    flexDirection : row
    height: ${Platform.OS == 'ios' ? 60 : 50}
    paddingTop: ${Platform.OS === 'ios' ? 15 : 0}
`;

export const HeaderLeft = styled.View`
    flex : 1
    marginLeft :5
    opacity : ${props => props.theme.opacity}
`;

export const HeaderTitle = styled.View`
    flex: 1
    justifyContent:center
    alignItems:center
`;

export const HeaderText = styled.Text`
    fontSize: 18
    color: white
    fontFamily:Merriweather-Regular
`;

export const HeaderRight = styled.Text`
    flex:1
`;