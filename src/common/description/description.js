import React from 'react';
import { Text , View } from 'react-native';
const Description = ({texttwo , textthree}) => {
    return (
         <View style={{flex:1 ,paddingHorizontal:10 , paddingBottom:10}}>
            <Text style={{textAlign:'center' ,color:'gray',fontFamily:'Merriweather-Regular'}}>{texttwo} <Text style={{textAlign:'center' ,color:'black'}}>{textthree}</Text></Text>
        </View>
    );
}

export default Description;