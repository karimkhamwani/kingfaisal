import {
    SHOW_LOADER,
    VIEWACTIVE_CHANGED
}
from '../HomeActions/ActionType.js'

import {
    YEARDETAILDATA_CHANGED,
    YEARDETAILVALUE_CHANGED,
    TEMP_CHANGED
} from './ActionType.js'

import { onGetYearDetailData } from '../../network/network'
export const onGetYearDetail = (selectedYear ,lang) => {
    return (dispatch) => {
        console.log('in first lineeeeeee')
        dispatch({ type: SHOW_LOADER, payload: true })
        dispatch({ type: VIEWACTIVE_CHANGED, payload: 'none' })
        console.log('Lang from year ac' , lang)
        onGetYearDetailData(selectedYear,lang)
            .then((res) => {
                console.log(res)
                if (res.data.status) {
                    dispatch({ type: YEARDETAILVALUE_CHANGED, payload: selectedYear })
                    dispatch({ type: SHOW_LOADER, payload: false })
                    dispatch({ type: VIEWACTIVE_CHANGED, payload: 'auto' })
                    dispatch({ type :TEMP_CHANGED , payload : res.data.response})
                    dispatch({ type :YEARDETAILDATA_CHANGED , payload : res.data.response})
                }
            },
            err => {
                dispatch({ type: SHOW_LOADER, payload: false })
                dispatch({ type: VIEWACTIVE_CHANGED, payload: 'auto' })
                console.log(err)
            })

    }
}