import {
    CONNECTION_CHANGED,
    USERNAME_CHANGED,
    SHOW_LOADER,
    VIEWACTIVE_CHANGED,
    YEARLISTDATA_CHANGED,
    YEARLISTDATA_CHANGEDAR
}
from './ActionType.js'

import {
    YEARDETAILDATA_CHANGED,
    YEARDETAILVALUE_CHANGED,
    TEMP_CHANGED
} from '../YearActions/ActionType.js'

import { onGetYearListData , onGetAllYears , onGetYearDetailData} from '../../network/network'
export const onConnectionChanged = (value) => {
    return {
        type: CONNECTION_CHANGED,
        payload: value
    }
}

export const onUsernameChanged = (value) => {
    return {
        type: USERNAME_CHANGED,
        payload: value
    }
}
export const onViewIsActiveChanged = (value) => {
    return {
        type: USERNAME_CHANGED,
        payload: value
    }
}

export const onGetYearList = (lang) => {
    return (dispatch) => {
        dispatch({ type: SHOW_LOADER, payload: true })
        dispatch({ type: VIEWACTIVE_CHANGED, payload: 'none' })
        onGetYearListData(lang)
            .then((res) => {
                console.log(res)
                if (res.data.status) {
                    dispatch({ type: VIEWACTIVE_CHANGED, payload: 'auto' })
                    // dispatch({ type: SHOW_LOADER, payload: false })
                    if(lang == 'en'){
                        dispatch({ type :YEARLISTDATA_CHANGED , payload : res.data.response})
                    }
                    else if(lang == 'ar'){
                        dispatch({ type :YEARLISTDATA_CHANGEDAR , payload : res.data.response})
                    }
                    
                }
            },
            err => {
                // dispatch({ type: VIEWACTIVE_CHANGED, payload: 'auto' })
                console.log(err)
            })

    }
}

export const getAllYears = (lang) => {
    return (dispatch) => {
        // dispatch({ type: SHOW_LOADER, payload: true })
        dispatch({ type: VIEWACTIVE_CHANGED, payload: 'none' })
        onGetAllYears(lang).
        then((res) => {
            var yearsarray = res.data.response.data
            console.log("years array is" ,yearsarray)
            var len = yearsarray.length
            console.log("years" ,yearsarray)
            yearsarray.map((year,index) => {
                return onGetYearDetailData(year,lang)
                .then((res) => {
                    dispatch({ type: YEARDETAILVALUE_CHANGED, payload: year })
                    dispatch({ type :TEMP_CHANGED , payload : res.data.response})
                    dispatch({ type :YEARDETAILDATA_CHANGED , payload : res.data.response})
                    console.log("index is " , index , len)
                    if(len-1 == index){
                        dispatch({ type: SHOW_LOADER, payload: false })
                        dispatch({ type: VIEWACTIVE_CHANGED, payload: 'auto' })
                    }
                },
                err => {
                    dispatch({ type: VIEWACTIVE_CHANGED, payload: 'auto' })
                    dispatch({ type: SHOW_LOADER, payload: false })
                    console.log(err)
                })
            })
            
        },
        err => {
            console.log(err)
        })
    }
}