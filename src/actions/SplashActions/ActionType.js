export const ANIMATION_CHANGED = "animation_changed"
export const ITERATIONCOUNT_CHANGED = "iterationcount_changed"
export const IMAGE1LOADED_CHANGED = "image1loaded_changed"
export const IMAGE1FULLYLOADED_CHANGED = "image1fullyloaded_changed"
export const IMAGE_CHANGED = "image_changed"

