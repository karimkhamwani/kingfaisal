import { 
    ANIMATION_CHANGED,
    ITERATIONCOUNT_CHANGED,
    IMAGE1LOADED_CHANGED,
    IMAGE1FULLYLOADED_CHANGED,
    IMAGE_CHANGED
} from './ActionType.js'


export const animationChanged = (value) => {
    return {
        type : ANIMATION_CHANGED,
        payload : value
    };
};

export const iterationCountChanged = (value) => {
    return {
        type : ITERATIONCOUNT_CHANGED,
        payload : value
    };
};

export const image1LoadedChanged = (value) => {
    return {
        type : IMAGE1LOADED_CHANGED,
        payload : value
    };
};

export const image1LoadedFullyChanged = (value) => {
    return {
        type : IMAGE1FULLYLOADED_CHANGED,
        payload : value
    };
};

export const imageChanged = (value) => {
    return {
        type : IMAGE_CHANGED,
        payload : value
    };
};