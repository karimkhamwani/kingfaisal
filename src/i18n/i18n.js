import ReactNative from 'react-native';
import I18n from 'react-native-i18n';
import { I18nManager } from 'react-native';

// Import all locales
import en from './en.json';
import ar from './ar.json';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;


// Define the supported translations
I18n.translations = {
  en,
  ar
};
I18n.locale = 'en'

// const currentLocale = I18n.currentLocale();

// Is it a RTL language?
// export const isRTL = currentLocale.indexOf('he') === 0 || currentLocale.indexOf('ar') === 0;

// Allow RTL alignment in RTL languages

I18nManager.forceRTL(false);
I18nManager.allowRTL(false);

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
};

export function getCurrentLocale() {
    return I18n.currentLocale();
}

export default I18n;